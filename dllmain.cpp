#include "stdafx.h"

#define DATAFEED_API __declspec(dllexport)

BOOL APIENTRY DllMain( 
    HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
    )
{
    return TRUE;
}

DATAFEED_API CFeedInterface * DsCreate()
{
    return new Feeder();
}

DATAFEED_API void DsDestroy(CFeedInterface * feed)
{
    delete (Feeder*) feed;
}

DATAFEED_API FeedDescription * DsVersion() 
{ 
    static FeedDescription description =
    {
        110,                                            // version
        "bitcoin-mtgox",
        "Timur Latypoff Technology Lab, 2013",
        "http://www.latypoff.com/",
        "support@latypoff.com",
        "",                                             // communicating server
        "",                                             // default login
        "",                                             // default password
        modeOnlyQuotes,
        //---- feeder short description
        "bitcoin-mtgox-feed\n"
        "Timur Latypoff Technology Lab, 2013, http://www.latypoff.com/\n"
        "Sources available here: https://bitbucket.org/latypoff/bitcoin-mtgox-feed\n\n"
        "Provides Bitcoin quotes from MtGox\n"
        "No configuration needed\n"
        "Supported symbols:\n"
        "BTCUSD, BTCEUR, BTCRUB,\n"
        "BTCJPY, BTCCAD, BTCAUD,\n"
        "BTCGBP, BTCSGD, BTCNZD,\n"
        "BTCSEK, BTCNOK, BTCCHF,\n"
        "BTCPLN, BTCCNY, BTCHKD,\n"
        "BTCTHB, BTCDKK.\n"
        "\n"
        "In Cryptography we trust",
        0
    };

    return &description; 
}
