#pragma once


class MtgoxHTTPClient : public MtgoxClient
{
    boost::thread _http_loop;

    std::map<std::string, FeedTick> _ticks;
    std::mutex _ticks_sync;

public:
    MtgoxHTTPClient()
    {
        std::vector<std::string> currencies;
        boost::split(currencies, CURRENCIES, boost::is_any_of(","));

        _http_loop = boost::thread(
            boost::bind(&MtgoxHTTPClient::HTTPSTickLoop, this, currencies)
            );
    }

    virtual ~MtgoxHTTPClient()
    {
        Done(true);

        if (_http_loop.joinable())
        {
            _http_loop.join();
        }
    }

private:
    virtual void PopTicks(FeedData * data)
    {
        std::lock_guard<std::mutex> lock(_ticks_sync);

        data->ticks_count = 0;

        const std::map<std::string, FeedTick> ticks = _ticks;

        BOOST_FOREACH (const auto & tick, ticks)
        {
            data->ticks[data->ticks_count++] = tick.second;
            _ticks.erase(tick.first);
        }
    }

    void PushTick(const std::string & symbol, double bid, double ask)
    {
        std::lock_guard<std::mutex> lock(_ticks_sync);

        FeedTick tick = {0};

        if (PrepareTick(tick, symbol, bid, ask))
        {
            _ticks[symbol] = tick;
        }    
    }

    void HTTPSTickLoop(const std::vector<std::string> currencies)
    {
        HINTERNET hInternet = ::InternetOpen(
            "MT4 data feed", 
            INTERNET_OPEN_TYPE_PRECONFIG, 
            NULL, 
            NULL,
            0
            );

        if (hInternet) 
        {
            HINTERNET hConnect = ::InternetConnect(
                hInternet, 
                "data.mtgox.com", 
                INTERNET_DEFAULT_HTTPS_PORT,
                NULL,
                NULL, 
                INTERNET_SERVICE_HTTP, 
                0,
                0
                );
        
            if (hConnect)
            {
                while (!Done() && HTTPSGetTicks(currencies, hConnect))
                {
                    if (!QuoteCooldown())
                    {
                        break;
                    }
                }

                ::InternetCloseHandle(hConnect);
            }

            ::InternetCloseHandle(hInternet);
        }

        Done(true);
    }

    bool QuoteCooldown() const
    {
        for (int i = 0; i < 300; ++i)
        {
            boost::this_thread::sleep_for(boost::chrono::milliseconds(50));

            if (Done())
            {
                return false;
            }
        }

        return true;
    }

    bool HTTPSGetTicks(const std::vector<std::string>& currencies, HINTERNET hConnect)
    {
        BOOST_FOREACH (const auto & currency, currencies)
        {
            if (!HTTPSGetTick(currency, hConnect))
            {
                return false;
            }
        }

        return true;
    }

    bool HTTPSGetTick(const std::string currency, const HINTERNET hConnect)
    {
        bool result = false;

        char szData[1024] = {0};
        std::string message;

        const std::string symbol = "BTC" + currency;

        std::string request = "/api/2/" + symbol + "/money/ticker_fast";
            
        HINTERNET hRequest = ::HttpOpenRequest(
            hConnect, 
            "GET", 
            request.c_str(),
            NULL,
            NULL,
            0, 
            INTERNET_FLAG_KEEP_CONNECTION | INTERNET_FLAG_SECURE,
            0
            );

        if (hRequest) 
        {   
            if (::HttpSendRequest(hRequest, NULL, 0, NULL, 0))
            {
                while (true)
                {
                    DWORD dwByteRead;
                    BOOL isRead = ::InternetReadFile(hRequest, szData, sizeof(szData) - 1, &dwByteRead);

                    if (!isRead || dwByteRead <= 0)
                    {
                        break;
                    }

                    szData[dwByteRead] = 0;

                    message.append(szData, dwByteRead);
                }

                try
                {
                    ParseMessage(message, symbol);
                    result = true;
                }
                catch (std::exception & ex)
                {
                    std::cout << ex.what() << std::endl;
                }
            }

            ::InternetCloseHandle(hRequest);
        }

        return result;
    }

    void ParseMessage(const std::string message, const std::string & symbol)
    {
        boost::property_tree::ptree message_pt = ConstructPtFromString(message);

        PushTick(
            symbol, 
            message_pt.get<double>("data.buy.value"), 
            message_pt.get<double>("data.sell.value")
            );
    }

};
